sprkwd.github.io
================
We make small, beautiful software.

Sparkwood and 21 was set up in 2009 by Duncan McKean when he wanted to have a business name to freelance under. He mostly offered graphic design, plus a bit of web and product design for clients such as: Elsevier, David Lynch Foundation, University of the West of England, Bristol Design Forge, IKEA, and so on and so forth. He also had offices in Amsterdam and Bristol.

Since 2017 the remit for Sparkwood and 21 has changed. No longer a freelance graphic designer Duncan is now concentrating on using design + technology to help the world become a better place.

Please don't get in touch with him, he's not good with people.


Go check us out at: http://sparkwoodand21.com
